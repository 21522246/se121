package com.se121.solid.liskov.substitution;

public interface Shape {
    public int computeArea();
}
