package com.se121.solid.interfaces.segregation.service;


import com.se121.solid.interfaces.segregation.entity.Entity;

//common interface to be implemented by all persistence services.
public interface PersistenceService<T extends Entity> {

	public void save(T entity);
	
	public void delete(T entity);
	
	public T findById(Long id);
	
}
