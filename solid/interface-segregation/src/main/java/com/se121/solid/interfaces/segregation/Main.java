package com.se121.solid.interfaces.segregation;

import com.se121.solid.interfaces.segregation.entity.User;
import com.se121.solid.interfaces.segregation.service.UserPersistenceService;

import java.time.LocalDateTime;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        UserPersistenceService userPersistenceService = new UserPersistenceService();

        userPersistenceService.save(createUser("user1"));
        userPersistenceService.save(createUser("user2"));
        userPersistenceService.save(createUser("user3"));


        List<User> users = userPersistenceService.findByName("user");
        for (User user:
             users) {
            System.out.println(user.getName());
        }
    }

    private static long ID = 1;
    public static User createUser(String name) {
        User user = new User();
        user.setName(name);
        user.setId(ID++);
        user.setLastLogin(LocalDateTime.now());

        return user;
    }
}
