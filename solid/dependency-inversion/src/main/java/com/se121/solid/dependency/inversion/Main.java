package com.se121.solid.dependency.inversion;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Main {

	public static void main(String[] args) throws IOException {

		Message msg = new Message("This is a new message again");
		MessagePrinter printer = new MessagePrinter();

		Formatter formatter = new JSONFormatter();
		String basePath = "/run/media/uit/Learning/CLASS/HK5/SE121/code/se121/solid/dependency-inversion/src/main/java/com/se121/solid/dependency/inversion/";
		try(PrintWriter writer = new PrintWriter(new FileWriter(basePath + "test_msg.json"))) { //creates print writer
			printer.writeMessage(msg, formatter, writer);
		}

	}

}
