package com.se121.solid.single.responsibility;

public class UserPersistenceService {
    //Store used by controller
    private final Store store = new Store();

    public void saveUser(User user) {
        store.store(user);
    }
}
