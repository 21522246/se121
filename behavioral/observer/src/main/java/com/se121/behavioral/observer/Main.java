package com.se121.behavioral.observer;

public class Main {

    public static void main(String[] args) {
        Order order = new Order("orderID");
        PriceObserver price = new PriceObserver();

        order.attach(price);

        order.addItem(70);
        order.addItem(20);

        System.out.println(order);
    }
}
