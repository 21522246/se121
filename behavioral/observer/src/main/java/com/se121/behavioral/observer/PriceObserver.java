package com.se121.behavioral.observer;

//Concrete observer
public class PriceObserver implements OrderObserver {

    @Override
    public void updated(Order order) {
        double total = order.getTotal();

        if (total >= 500) {
            order.setDiscount(20);
        } else {
            order.setDiscount(10);
        }
    }
}
