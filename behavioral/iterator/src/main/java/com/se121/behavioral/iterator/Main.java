package com.se121.behavioral.iterator;

import com.sun.source.tree.WhileLoopTree;

public class Main {

	public static void main(String[] args) {
		Iterator iterator = ThemeColor.getIterator();
		while (iterator.hasNext()) {
			System.out.println(iterator.next());
		}
	}

}
