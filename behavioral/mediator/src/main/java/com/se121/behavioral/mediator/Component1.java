package com.se121.behavioral.mediator;

public class Component1 extends BaseComponent{
    public void doSomethingA() {
        System.out.println("Component 1 does A.");
        this.mediator.notify(this, "A");
    }
    public void doSomethingB() {
        System.out.println("Component 1 does B.");
        this.mediator.notify(this, "B");
    }

}
