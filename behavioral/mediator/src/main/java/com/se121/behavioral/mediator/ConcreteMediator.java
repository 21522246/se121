package com.se121.behavioral.mediator;

public class ConcreteMediator implements Mediator{
    private Component1 component1;
    private Component2 component2;

    public ConcreteMediator(Component1 component1, Component2 component2) {
        this.component1 = component1;
        this.component1.setMediator(this);

        this.component2 = component2;
        this.component2.setMediator(this);
    }

    @Override
    public void notify(Object sender, String ev) {
        switch (ev){
            case "A":
                System.out.println("Mediator reacts on A and triggers folowing operations:");
                component2.doSomethingC();
                return;
            case "D":
                System.out.println("Mediator reacts on D and triggers folowing operations:");
                component1.doSomethingB();
                component2.doSomethingC();
                return;
        }
    }
}
