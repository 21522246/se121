package com.se121.behavioral.mediator;

public interface Mediator {
    void notify(Object sender, String ev);
}
