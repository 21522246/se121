package com.se121.behavioral.nulls.object;

public class Report {

	private String name;
	
	public Report(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}
