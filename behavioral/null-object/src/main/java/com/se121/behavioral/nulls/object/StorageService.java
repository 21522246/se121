package com.se121.behavioral.nulls.object;

import java.io.IOException;
import java.io.PrintWriter;

public class StorageService {

	public void save(Report report) {
		System.out.println("Writing report out");
		String path	= "/run/media/uit/Learning/CLASS/HK5/SE121/code/se121/behavioral/null-object/src/main/java/com/se121/behavioral/nulls/object/";
		try(PrintWriter writer = new PrintWriter(path + report.getName()+".txt")) {
			
			writer.println(report.getName());
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
