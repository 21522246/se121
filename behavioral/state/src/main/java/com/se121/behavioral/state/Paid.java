package com.se121.behavioral.state;

public class Paid implements OrderState {

    @Override
    public double handleCancellation() {
        System.out.println("Contacting payment gateway to roll back transaction");
        return 10;
    }
}
