package com.se121.creational.factory.method;

import com.se121.creational.factory.method.message.Message;
import com.se121.creational.factory.method.message.TextMessage;

/**
 * Provides implementation for creating Text messages
 */
public class TextMessageCreator extends MessageCreator {

    @Override
    public Message creatMessage() {
        return new TextMessage();
    }



}
