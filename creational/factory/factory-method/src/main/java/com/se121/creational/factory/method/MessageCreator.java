package com.se121.creational.factory.method;

import com.se121.creational.factory.method.message.Message;

/**
 * This is our abstract "creator". 
 * The abstract method createMessage() has to be implemented by
 * its subclasses.
 */
public abstract class MessageCreator {

	public Message getMessage() {
        Message message = creatMessage();

        message.addDefaultHeaders();
        message.encrypt();

        return message;
    }

    public abstract Message creatMessage();
}
