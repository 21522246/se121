package com.se121.creational.factory.method;

// Product interface
interface Product {
    void operation();
}

// ConcreteProduct
class ConcreteProduct implements Product {
    @Override
    public void operation() {
        System.out.println("ConcreteProduct operation");
    }
}

// Creator interface
interface Creator {
    Product factoryMethod();
}

// ConcreteCreator
class ConcreteCreator implements Creator {
    @Override
    public Product factoryMethod() {
        return new ConcreteProduct();
    }
}

// Client class
class Client {
    private final Creator creator;

    public Client(Creator creator) {
        this.creator = creator;
    }

    public void performOperation() {
        Product product = creator.factoryMethod();
        product.operation();
    }
}

// Main class for testing
public class Diagram {
    public static void main(String[] args) {
        Creator creator = new ConcreteCreator();
        Client client = new Client(creator);
        client.performOperation();
    }
}
