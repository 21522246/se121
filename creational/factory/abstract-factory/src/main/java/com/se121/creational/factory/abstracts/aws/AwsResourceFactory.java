package com.se121.creational.factory.abstracts.aws;

import com.se121.creational.factory.abstracts.Instance;
import com.se121.creational.factory.abstracts.ResourceFactory;
import com.se121.creational.factory.abstracts.Storage;

//Factory implementation for Google cloud platform resources
public class AwsResourceFactory implements ResourceFactory {

	@Override
	public Instance createInstance(Instance.Capacity cap) {
		return new Ec2Instance(cap);
	}

	@Override
	public Storage createStorage(int storageMib) {
		return new S3Storage(storageMib);
	}
}
