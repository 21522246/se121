package com.se121.creational.factory.abstracts.gcp;

import com.se121.creational.factory.abstracts.Instance;
import com.se121.creational.factory.abstracts.Instance.Capacity;
import com.se121.creational.factory.abstracts.ResourceFactory;
import com.se121.creational.factory.abstracts.Storage;

//Factory implementation for Google cloud platform resources
public class GoogleResourceFactory implements ResourceFactory {

	@Override
	public Instance createInstance(Capacity cap) {
		return new GoogleComputeEngineInstance(cap);
	}

	@Override
	public Storage createStorage(int storageMib) {
		return new GoogleCloudStorage(storageMib);
	}
}
