package com.se121.creational.factory.abstracts;

//Represents an abstract product
public interface Storage {
    String getId();

}
