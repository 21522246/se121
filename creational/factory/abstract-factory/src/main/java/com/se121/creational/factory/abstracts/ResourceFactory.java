package com.se121.creational.factory.abstracts;

//Abstract factory with methods defined for each object type.
public interface ResourceFactory {

	Instance createInstance(Instance.Capacity cap);

	Storage createStorage(int storageMib);
}
