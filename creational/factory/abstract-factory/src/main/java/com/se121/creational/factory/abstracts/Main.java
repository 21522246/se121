package com.se121.creational.factory.abstracts;


import com.se121.creational.factory.abstracts.aws.AwsResourceFactory;
import com.se121.creational.factory.abstracts.gcp.GoogleResourceFactory;

public class Main {
	private ResourceFactory factory;
	public Main(ResourceFactory factory) {
		this.factory = factory;
	}

	public Instance createServer(Instance.Capacity capacity, int storageMib) {
		Instance instance = factory.createInstance(capacity);
		Storage storage = factory.createStorage(storageMib);
		instance.attachStorage(storage);

		return instance;
	}


    public static void main(String[] args) {
    	Main aws = new Main(new AwsResourceFactory());
		Instance i1 = aws.createServer(Instance.Capacity.micro, 2048);
		i1.start();
		i1.stop();

		System.out.println("************************************88");

		Main gcp = new Main(new GoogleResourceFactory());
		Instance i2 = gcp.createServer(Instance.Capacity.micro, 1024);
		i2.start();
		i2.stop();

    }
}
