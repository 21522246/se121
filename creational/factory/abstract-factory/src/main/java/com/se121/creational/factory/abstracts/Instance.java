package com.se121.creational.factory.abstracts;

//Represents an abstract product
public interface Instance {
    enum Capacity{micro, small, large}
    void start();
    void attachStorage(Storage storage);
    void stop();
}
