package com.se121.creational.object.pool;

import javafx.geometry.Point2D;

public class Main {
    public static final ObjectPool<Bitmap> bitmapPool = new ObjectPool<>(() -> new Bitmap("Name.bmp"), 2);
    public static void main(String[] args) throws InterruptedException {
        Bitmap b1 = bitmapPool.get();
        Bitmap b2 = bitmapPool.get();
        Thread thread = new Thread(() -> {
            try {
                System.out.println("Releasing.. b1");
                Thread.sleep(5000);
                bitmapPool.release(b1);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });
        thread.start();

        Bitmap b3 = bitmapPool.get();
        b3.setLocation(new Point2D(1,1));
        b3.draw();

        bitmapPool.release(b2);
        bitmapPool.release(b3);
        thread.join();
    }
}
