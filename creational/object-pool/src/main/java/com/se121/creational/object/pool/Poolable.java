package com.se121.creational.object.pool;

public interface Poolable {

	//state reset
	void reset();
}
