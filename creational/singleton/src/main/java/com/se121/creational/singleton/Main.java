package com.se121.creational.singleton;

public class Main {

	public static void main(String[] args) throws InterruptedException {
		checkEagerRegistry();
		checkLazyRegistryWithDCL();
		checkLazyRegistryIODH();
	}

	private static void checkLazyRegistryIODH() {
		System.out.println();
		LazyRegistryIODH registry;
		System.out.println("Not calling constructor yet");
		registry = LazyRegistryIODH.getInstance();
	}

	private static void checkLazyRegistryWithDCL() throws InterruptedException {
		System.out.println();
		Thread thread = new Thread(() -> {
			LazyRegistryWithDCL registryTest = LazyRegistryWithDCL.getInstance("thread");
		});
		thread.start();
		LazyRegistryWithDCL registry = LazyRegistryWithDCL.getInstance("registry");
		thread.join();
		LazyRegistryWithDCL registry2 = LazyRegistryWithDCL.getInstance("registry2");
		System.out.println(registry == registry2);
	}

	private static void checkEagerRegistry() {
		System.out.println();
		EagerRegistry registry = EagerRegistry.getInstance();
		EagerRegistry registry2 = EagerRegistry.getInstance();
		System.out.println(registry == registry2);
	}

}
