package com.se121.creational.prototype;

import javafx.geometry.Point3D;

public class Main {

	public static void main(String[] args) throws CloneNotSupportedException {
		Swordsman s1 = new Swordsman();
		s1.move(new Point3D(1,0,0), 10);
		s1.attack();

		System.out.println(s1);

		Swordsman s2 = (Swordsman) s1.clone();
		System.out.println("Cloned swordsman \n"+s2);

	}

}
