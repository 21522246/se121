package com.se121.creational.builder.v1;

//Interface implemented by "products"
public interface UserDTO {
	
	String getName();

	String getAddress();

	String getAge();
}