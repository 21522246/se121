package com.se121.creational.builder;

// Product class
class Product {
    private String part1;
    private String part2;

    public void setPart1(String part1) {
        this.part1 = part1;
    }

    public void setPart2(String part2) {
        this.part2 = part2;
    }

    public void show() {
        System.out.println("Part 1: " + part1);
        System.out.println("Part 2: " + part2);
    }
}

// Builder interface
interface Builder {
    void buildPart1();
    void buildPart2();
    Product getResult();
}

// ConcreteBuilder class
class ConcreteBuilder implements Builder {
    private Product product = new Product();

    @Override
    public void buildPart1() {
        product.setPart1("Builder Part 1");
    }

    @Override
    public void buildPart2() {
        product.setPart2("Builder Part 2");
    }

    @Override
    public Product getResult() {
        return product;
    }
}

// Director class
class Director {
    public void construct(Builder builder) {
        builder.buildPart1();
        builder.buildPart2();
    }
}

// Client class
public class Client {
    public void performOperation() {
        Builder builder = new ConcreteBuilder();
        Director director = new Director();

        director.construct(builder);

        Product product = builder.getResult();
        product.show();
    }
}
