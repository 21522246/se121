package com.se121.structural.proxy.dynamic;


import com.se121.structural.proxy.Image;
import javafx.geometry.Point2D;

public class Main {

	public static void main(String[] args) {
		Image img = ImageFactory.getImage("A.bmp");
		img.setLocation(new Point2D(1, 0));
		System.out.println(img.getLocation());
		System.out.println("*****************************");
		img.render();
		
	}
}
