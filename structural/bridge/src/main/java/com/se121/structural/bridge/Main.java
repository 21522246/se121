package com.se121.structural.bridge;

public class Main {

	public static void main(String[] args) {
		testInteger();
		testString();
	}

	public static void testInteger() {
		FifoCollection<Integer> collection = new Queue<>(new SinglyLinkedList<>());
		collection.offer(1);
		collection.offer(2);
		collection.offer(3);

		System.out.println(collection.poll());
		System.out.println(collection.poll());
		System.out.println(collection.poll());
		System.out.println(collection.poll());
	}

	public static void testString() {
		FifoCollection<String> collection = new Queue<>(new SinglyLinkedList<>());
		collection.offer("test 1");
		collection.offer("test 2");
		collection.offer("test 3");

		System.out.println(collection.poll());
		System.out.println(collection.poll());
		System.out.println(collection.poll());
		System.out.println(collection.poll());
	}

}
